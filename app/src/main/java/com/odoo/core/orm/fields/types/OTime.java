package com.odoo.core.orm.fields.types;

import com.odoo.core.utils.ODateUtils;

/**
 * Created by mohitraheja on 19/09/15.
 */
public class OTime extends OTypeHelper {
    public static final String TAG = OTime.class.getSimpleName();

    @Override
    public String getFieldType() {
        return "VARCHAR";
    }

    @Override
    public String getDataFormat() {
        return ODateUtils.DEFAULT_TIME_FORMAT;
    }
}
