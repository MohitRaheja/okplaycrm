package com.odoo.addons.dsr.models;

import android.content.Context;
import android.net.Uri;

import com.odoo.addons.crm.models.*;
import com.odoo.addons.phonecall.models.CRMPhoneCalls;
import com.odoo.base.addons.res.ResPartner;
import com.odoo.base.addons.res.ResUsers;
import com.odoo.core.orm.OModel;
import com.odoo.core.orm.OValues;
import com.odoo.core.orm.annotation.Odoo;
import com.odoo.core.orm.fields.OColumn;
import com.odoo.core.orm.fields.types.OBoolean;
import com.odoo.core.orm.fields.types.ODate;
import com.odoo.core.orm.fields.types.ODateTime;
import com.odoo.core.orm.fields.types.OInteger;
import com.odoo.core.orm.fields.types.OSelection;
import com.odoo.core.orm.fields.types.OText;
import com.odoo.core.orm.fields.types.OTime;
import com.odoo.core.orm.fields.types.OTimestamp;
import com.odoo.core.orm.fields.types.OVarchar;
import com.odoo.core.support.OUser;

import odoo.ODomain;
import odoo.controls.ODateTimeField;

/**
 * Created by mohitraheja on 21/08/15.
 */
public class DSRModel extends OModel{

    public static final String TAG = DSRModel.class.getSimpleName();
    public static final String AUTHORITY = "com.odoo.core.crm.provider.content.sync.dsr_event";
    private Context mContext;

    OColumn salesman_id = new OColumn("Salesman Id", OInteger.class);

    OColumn customer_name = new OColumn("Customer Name", OText.class);

    OColumn date = new OColumn("Date", ODate.class);

    OColumn in_time = new OColumn("In Time", OTime.class).setRequired();

    OColumn out_time = new OColumn("Out Time", OTime.class).setRequired();

    OColumn existing_new = new OColumn("Existing/New", OSelection.class)
            .addSelection("new", "New")
            .addSelection("existing", "Existing")
            .setDefaultValue("existing");

    OColumn cus_address = new OColumn("Address", OVarchar.class);

    OColumn customer_id = new OColumn("Customer", ResPartner.class,
            OColumn.RelationType.ManyToOne).addDomain("customer", "=", "true");

    OColumn email = new OColumn("Email", OVarchar.class).setSize(128);

    OColumn phone_no = new OColumn("Phone", OVarchar.class).setSize(20);

    OColumn order_value = new OColumn("Order Value", OVarchar.class).setSize(20);

    // PhoneCalls link
    OColumn calls = new OColumn("Calls", OSelection.class)
            .addSelection("productive", "Productive")
            .addSelection("unproductive", "Unproductive")
            .setDefaultValue("productive");

    OColumn remarks = new OColumn("Remarks", OText.class);


    public DSRModel(Context context, OUser user) {
        super(context, "dsr.crm", user);
        mContext = context;
        //setHasMailChatter(true);
    }

    @Override
    public Uri uri() {

        return buildURI(AUTHORITY);
    }

    @Override
    public ODomain defaultDomain() {
        ODomain domain = new ODomain();
//        domain.add("user_id", "=", getUser().getUser_id());
        return domain;
    }

}
