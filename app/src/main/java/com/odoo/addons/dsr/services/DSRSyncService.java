/**
 * Odoo, Open Source Management Solution
 * Copyright (C) 2012-today Odoo SA (<http:www.odoo.com>)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http:www.gnu.org/licenses/>
 *
 * Created on 13/1/15 11:31 AM
 */
package com.odoo.addons.dsr.services;

import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;

import com.odoo.addons.dsr.models.DSRModel;
import com.odoo.core.service.ISyncFinishListener;
import com.odoo.core.service.OSyncAdapter;
import com.odoo.core.service.OSyncService;
import com.odoo.core.support.OUser;

import odoo.ODomain;

public class DSRSyncService extends OSyncService implements ISyncFinishListener {
    public static final String TAG = DSRSyncService.class.getSimpleName();
    private Context mContext;
    private OSyncService service;

    @Override
    public OSyncAdapter getSyncAdapter(OSyncService service, Context context) {
        mContext = context;
        this.service = service;
        return new OSyncAdapter(context, DSRModel.class, service, true);
    }

    @Override
    public void performDataSync(OSyncAdapter adapter, Bundle extras, OUser user) {
        if (adapter.getModel().getModelName().equals("dsr.crm")) {
            adapter.onSyncFinish(this).syncDataLimit(50);
        }
    }

    @Override
    public OSyncAdapter performNextSync(OUser user, SyncResult syncResult) {
        DSRModel crmLead = new DSRModel(mContext, user);
//        for (ODataRow row : crmLead.select(new String[]{})) {
//            crmLead.setReminder(row.getInt(OColumn.ROW_ID));
//        }
        return new OSyncAdapter(mContext, DSRModel.class, service, true);
    }
}
