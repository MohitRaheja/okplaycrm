package com.odoo.addons.dsr;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.odoo.App;
import com.odoo.R;
import com.odoo.addons.dsr.models.DSRModel;
import com.odoo.addons.dsr.services.DSRSyncService;
import com.odoo.base.addons.res.ResCompany;
import com.odoo.base.addons.res.ResPartner;
import com.odoo.base.addons.res.ResUsers;
import com.odoo.core.orm.ODataRow;
import com.odoo.core.orm.OModel;
import com.odoo.core.orm.OValues;
import com.odoo.core.orm.fields.OColumn;
import com.odoo.core.orm.fields.types.ODateTime;
import com.odoo.core.orm.fields.types.OSelection;
import com.odoo.core.utils.OActionBarUtils;
import com.odoo.core.utils.ODateUtils;

import java.util.Calendar;

import odoo.controls.ODateTimeField;
import odoo.controls.OField;
import odoo.controls.OForm;
import odoo.controls.OSelectionField;

/**
 * Created by mohitraheja on 21/08/15.
 */
public class DSRDetail extends ActionBarActivity implements OField.IOnFieldValueChangeListener {
    public static final String TAG = DSRDetail.class.getSimpleName();
    private Bundle extra;
    private OForm mForm;
    private ODataRow record;
    private DSRModel dsr_model;
    private ActionBar actionBar;
    private Menu menu;
    //private TextView currency_symbol;
    private int stage_id = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dsr_detail);
        OActionBarUtils.setActionBar(this, true);
        actionBar = getSupportActionBar();
        dsr_model = new DSRModel(this, null);
        extra = getIntent().getExtras();
        init();
    }

    private void init() {
        mForm = (OForm) findViewById(R.id.dsrForm);
//        ODateTimeField in_time = (ODateTimeField) findViewById(R.id.in_time);
//        ODateTimeField out_time = (ODateTimeField) findViewById(R.id.out_time);

//        in_time.setFieldType(OField.FieldType.Time);
//        out_time.setFieldType(OField.FieldType.Time);

        //currency_symbol = (TextView) findViewById(R.id.currency_symbol);
        if (!extra.containsKey(OColumn.ROW_ID)) {
            if (extra.containsKey("stage_id")) {
                stage_id = extra.getInt("stage_id");
            }
            mForm.initForm(null);
            actionBar.setTitle(R.string.label_tag_new);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_action_navigation_close);
//            ODataRow currency = ResCompany.getCurrency(this);
//            if (currency != null) {
//                currency_symbol.setText(currency.getString("symbol"));
//            }
        } else {
            initFormValues();
        }
        OField type = (OField) findViewById(R.id.type);
        type.setOnValueChangeListener(this);
        mForm.setEditable(true);

    }

    private void initFormValues() {
        record = dsr_model.browse(extra.getInt(OColumn.ROW_ID));
        if (record == null) {
            finish();
        }
        //ODataRow currency = record.getM2ORecord("company_currency").browse();
//        if (currency != null) {
//            currency_symbol.setText(currency.getString("symbol"));
//        }
        //if (!record.getString("type").equals("lead")) {
            actionBar.setTitle(R.string.label_dsrs);
//            type = "opportunity";
//            findViewById(R.id.opportunity_controls).setVisibility(View.VISIBLE);
//        } else {
//            actionBar.setTitle(R.string.label_lead);
//        }
        mForm.initForm(record);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dsr_detail, menu);
        this.menu = menu;
        toggleMenu();
        return true;
    }

    private void toggleMenu() {
//        if (!extra.containsKey(OColumn.ROW_ID)) {
//            menu.findItem(R.id.menu_lead_detail_more).setVisible(false);
//        } else {
//            initFormValues();
//            menu.findItem(R.id.menu_lead_detail_more).setVisible(true);
//            if (record.getString("type").equals(CRMLead.KEY_LEAD)) {
//                menu.findItem(R.id.menu_lead_convert_to_quotation).setVisible(false);
//                menu.findItem(R.id.menu_mark_won).setVisible(false);
//            } else if (record.getString("type").equals(crmLead.KEY_OPPORTUNITY)) {
//                menu.findItem(R.id.menu_lead_convert_to_opportunity).setVisible(false);
//            }
//        }
        menu.findItem(R.id.menu_lead_save).setVisible(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_action_navigation_close);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        App app = (App) getApplicationContext();
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_lead_save:
                OValues values = mForm.getValues();
                if (stage_id != OModel.INVALID_ROW_ID) {
                    values.put("stage_id", stage_id);
                }
                if (values != null) {
                    values.put("salesman_id", dsr_model.getUser().getUser_id());
                    if(values.getString("customer_name").equals("") || values.getString("customer_name").equals("false")) {
                        OField customer = (OField) findViewById(R.id.customer);
                        OSelectionField control = (OSelectionField) customer.getControlView();
                        ODataRow row = control.getRow();
                        String name = ResPartner.getName(row);

                        values.put("customer_name", name.substring(0, name.length() - 2));
                    }
                    int row_id;
                    if (record != null) {
                        dsr_model.update(record.getInt(OColumn.ROW_ID), values);
                        row_id = record.getInt(OColumn.ROW_ID);
                    } else {
                        /*values.put("company_id", ResCompany.myId(this));
                        values.put("company_currency", ResCompany.myCurrency(this));
                        values.put("create_date", ODateUtils.getUTCDate());
                        values.put("user_id", ResUsers.myId(this));
                        //CRMCaseStage stages = new CRMCaseStage(this, null);
                        //ODataRow row;
//                        if (!values.contains("stage_id")) {
//                            row = stages.browse(new String[]{"name"}, "name = ?", new String[]{"New"});
//                        } else {
//                            row = stages.browse(stage_id);
//                        }
//                        if (row != null) {
//                            values.put("stage_id", row.getInt(OColumn.ROW_ID));
//                            values.put("stage_name", row.getString("name"));
//                        }

                        values.put("display_name", values.getString("partner_name"));
                        values.put("assignee_name", dsr_model.getUser().getName());*/
                        values.put("date", ODateUtils.getDate(ODateUtils.DEFAULT_DATE_FORMAT));
                        row_id = dsr_model.insert(values);
                    }
                    //dsr_model.setReminder(row_id);
//                    startService(new Intent(this, DSRSyncService.class));
                    finish();

                }
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFieldValueChange(OField field, Object value) {
        if (field.getFieldName().equals("existing_new")) {
            String type = value.toString();
            if(type.equalsIgnoreCase("new")){
                findViewById(R.id.customer_name).setVisibility(View.VISIBLE);
                findViewById(R.id.customer).setVisibility(View.GONE);
                findViewById(R.id.type_seprator).setVisibility(View.VISIBLE);
            }
            else {
                findViewById(R.id.customer_name).setVisibility(View.GONE);
                findViewById(R.id.type_seprator).setVisibility(View.GONE);
                findViewById(R.id.customer).setVisibility(View.VISIBLE);
            }
        }
    }
}
