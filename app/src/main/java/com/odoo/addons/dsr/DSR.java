package com.odoo.addons.dsr;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.odoo.R;
import com.odoo.addons.crm.*;
import com.odoo.addons.crm.models.CRMLead;
import com.odoo.addons.customers.Customers;
import com.odoo.addons.dsr.models.DSRModel;
import com.odoo.base.addons.res.ResPartner;
import com.odoo.core.orm.ODataRow;
import com.odoo.core.orm.fields.OColumn;
import com.odoo.core.support.addons.fragment.BaseFragment;
import com.odoo.core.support.addons.fragment.IOnSearchViewChangeListener;
import com.odoo.core.support.addons.fragment.ISyncStatusObserverListener;
import com.odoo.core.support.drawer.ODrawerItem;
import com.odoo.core.support.list.IOnItemClickListener;
import com.odoo.core.support.list.OCursorListAdapter;
import com.odoo.core.utils.IntentUtils;
import com.odoo.core.utils.OAlert;
import com.odoo.core.utils.OControls;
import com.odoo.core.utils.OCursorUtils;
import com.odoo.core.utils.ODateUtils;
import com.odoo.core.utils.OResource;
import com.odoo.core.utils.StringUtils;
import com.odoo.core.utils.sys.IOnActivityResultListener;
import com.odoo.core.utils.sys.IOnBackPressListener;
import com.odoo.widgets.bottomsheet.BottomSheet;
import com.odoo.widgets.bottomsheet.BottomSheetListeners;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mohitraheja on 20/08/15.
 */
public class DSR extends BaseFragment implements OCursorListAdapter.OnViewBindListener,
        LoaderManager.LoaderCallbacks<Cursor>, SwipeRefreshLayout.OnRefreshListener,
        ISyncStatusObserverListener, OCursorListAdapter.BeforeBindUpdateData,
        IOnSearchViewChangeListener, View.OnClickListener, IOnItemClickListener,
        IOnBackPressListener{
    public static final String TAG = DSR.class.getSimpleName();
    public static final String KEY_MENU = "key_menu_item";
    private View mView;
    private int mLocal_id = 0;
    private ListView mList;
    private OCursorListAdapter mAdapter;
    private BottomSheet mSheet = null;
    private String mFilter = null;
    private String wonLost = "won";
    private boolean syncRequested = false;
    // Customer's data filter
    private boolean filter_customer_data = false;
    private int customer_id = -1;
    private ODataRow convertRequestRecord = null;
    private Bundle syncBundle = new Bundle();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
//        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
//            @Override
//            public void uncaughtException(Thread thread, Throwable ex) {
//                Log.e("excecptio...", ex.toString());
//            }
//        });
        return inflater.inflate(R.layout.common_listview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mView = view;
        parent().setOnBackPressListener(this);
//        parent().setOnActivityResultListener(this);
        Bundle extra = getArguments();
        if (extra != null && extra.containsKey(Customers.KEY_FILTER_REQUEST)) {
            filter_customer_data = true;
            customer_id = extra.getInt(Customers.KEY_CUSTOMER_ID);
            mView.findViewById(R.id.customer_filterContainer).setVisibility(View.VISIBLE);
            OControls.setText(mView, R.id.customer_name, extra.getString("name"));
            mView.findViewById(R.id.cancel_filter).setOnClickListener(this);
        }
        setHasSyncStatusObserver(TAG, this, db());
        initAdapter();
    }

    private void initAdapter() {
        mList = (ListView) mView.findViewById(R.id.listview);
        mAdapter = new OCursorListAdapter(getActivity(), null, R.layout.dsr_item);
        mAdapter.setOnViewBindListener(this);
        mList.setAdapter(mAdapter);
        setHasFloatingButton(mView, R.id.fabButton, mList, this);
        mAdapter.handleItemClickListener(mList, this);
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.fabButton:
                Bundle type = new Bundle();
                IntentUtils.startActivity(getActivity(), com.odoo.addons.dsr.DSRDetail.class, type);
                break;
        }
    }

    @Override
    public ODataRow updateDataRow(Cursor cr) {
        return db().browse(new String[]{"stage_id"},
                cr.getInt(cr.getColumnIndex(OColumn.ROW_ID)));
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle data) {
//        String where = " type = ?";
//        String[] whereArgs;
//        List<String> args = new ArrayList<>();
//        //args.add("lead");
//        if (mFilter != null) {
//            where += " and (name like ? or description like ? or display_name like ? " +
//                    "or stage_name like ? or title_action like ?)";
//            args.add("%" + mFilter + "%");
//            args.add("%" + mFilter + "%");
//            args.add("%" + mFilter + "%");
//            args.add("%" + mFilter + "%");
//            args.add("%" + mFilter + "%");
//        }
//        if (filter_customer_data) {
//            where += " and partner_id = ?";
//            args.add(customer_id + "");
//        }
//        whereArgs = args.toArray(new String[args.size()]);

        return new CursorLoader(getActivity(), db().uri(), null, null, null, "date DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.changeCursor(data);
        if (data != null && data.getCount() > 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    OControls.setGone(mView, R.id.loadingProgress);
                    OControls.setVisible(mView, R.id.swipe_container);
                    OControls.setGone(mView, R.id.customer_no_items);
                    setHasSwipeRefreshView(mView, R.id.swipe_container, DSR.this);
                }
            }, 500);
        } else {
            if (db().isEmptyTable() && !syncRequested) {
                syncRequested = true;
                onRefresh();
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    OControls.setGone(mView, R.id.loadingProgress);
                    OControls.setGone(mView, R.id.swipe_container);
                    OControls.setVisible(mView, R.id.customer_no_items);
                    setHasSwipeRefreshView(mView, R.id.customer_no_items, DSR.this);
                    OControls.setImage(mView, R.id.icon, R.drawable.ic_action_leads
                    );
                    OControls.setText(mView, R.id.title, "No Dsr's Found");
                    OControls.setText(mView, R.id.subTitle, "");
                }
            }, 500);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.changeCursor(null);
    }

    @Override
    public void onViewBind(View view, Cursor cursor, ODataRow row) {
        OControls.setText(view, R.id.customer_name, row.getString("customer_name"));
        OControls.setText(view, R.id.order_value, row.getString("order_value"));
        String date = ODateUtils.convertToDefault(row.getString("date"),
                ODateUtils.DEFAULT_DATE_FORMAT, "MMMM, dd");
        OControls.setText(view, R.id.create_date, date);
        //view.findViewById(R.id.opportunity_controls).setVisibility(View.GONE);
    }

    @Override
    public List<ODrawerItem> drawerMenus(Context context) {
        List<ODrawerItem> menu = new ArrayList<>();
        menu.add(new ODrawerItem(TAG)
                .setTitle(OResource.string(context, R.string.label_dsrs))
                .setInstance(new DSR())
                .setIcon(R.drawable.ic_action_leads));
        return menu;
    }

    @Override
    public Class<DSRModel> database() {
        return DSRModel.class;
    }

    @Override
    public void onRefresh() {
        if (inNetwork()) {
            parent().sync().requestSync(DSRModel.AUTHORITY, syncBundle);
            setSwipeRefreshing(true);
        } else {
            hideRefreshingProgress();
            Toast.makeText(getActivity(), _s(R.string.toast_network_required), Toast.LENGTH_LONG)
                    .show();
        }
    }

    @Override
    public void onStatusChange(Boolean refreshing) {
        getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_dsr, menu);
        setHasSearchView(this, menu, R.id.menu_dsr_search);
    }

    @Override
    public boolean onSearchViewTextChange(String newFilter) {
        mFilter = newFilter;
        getLoaderManager().restartLoader(0, null, this);
        return true;
    }

    @Override
    public void onSearchViewClose() {
        // Nothing to do
    }

    @Override
    public void onItemDoubleClick(View view, int position) {
        ODataRow row = OCursorUtils.toDatarow((Cursor) mAdapter.getItem(position));
        IntentUtils.startActivity(getActivity(), com.odoo.addons.crm.CRMDetail.class, row.getPrimaryBundleData());
    }

    @Override
    public void onItemClick(View view, int position) {
//        ODataRow row = OCursorUtils.toDatarow((Cursor) mAdapter.getItem(position));
//        if (row.getInt(OColumn.ROW_ID) == 0) {
//            DSRQuickCreater customerQuickCreater =
//                    new DSRQuickCreater(new OnLiveSearchRecordCreateListener() {
//                        @Override
//                        public void recordCreated(ODataRow row) {
//                            Cursor cr = getActivity().getContentResolver()
//                                    .query(db().uri(), null, "id = ?", new String[]{row.getString("id")}
//                                            , null);
//                            cr.moveToFirst();
//                            showSheet(cr);
//                        }
//                    });
//            customerQuickCreater.execute(row);
//        } else
//            showSheet((Cursor) mAdapter.getItem(position));
        ODataRow row = OCursorUtils.toDatarow((Cursor) mAdapter.getItem(position));
        IntentUtils.startActivity(getActivity(), com.odoo.addons.dsr.DSRDetail.class, row.getPrimaryBundleData());
    }

    private void showSheet(Cursor data) {
        BottomSheet.Builder builder = new BottomSheet.Builder(getActivity());
//        builder.listener(this);
        builder.setIconColor(_c(R.color.body_text_2));
        builder.setTextColor(_c(R.color.body_text_2));
        builder.setData(data);
//        builder.actionListener(this);
        builder.setActionIcon(R.drawable.ic_action_edit);
        builder.title(data.getString(data.getColumnIndex("customer_name")));
        builder.menu(R.menu.menu_lead_list_sheet);
        mSheet = builder.create();
        mSheet.show();
    }

    @Override
    public boolean onBackPressed() {
        if (mSheet != null && mSheet.isShowing()) {
            mSheet.dismiss();
            return false;
        }
        return true;
    }

    private class DSRQuickCreater extends AsyncTask<ODataRow, Void, ODataRow> {
        private ProgressDialog progressDialog;
        private OnLiveSearchRecordCreateListener mOnLiveSearchRecordCreateListener;

        public DSRQuickCreater(OnLiveSearchRecordCreateListener listener) {
            mOnLiveSearchRecordCreateListener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle(R.string.title_working);
            progressDialog.setMessage(_s(R.string.title_please_wait));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected ODataRow doInBackground(ODataRow... params) {
            try {
                Thread.sleep(500);
                return db().quickCreateRecord(params[0]);
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(ODataRow row) {
            super.onPostExecute(row);
            progressDialog.dismiss();
            getLoaderManager().restartLoader(0, null, DSR.this);
            if (mOnLiveSearchRecordCreateListener != null && row != null) {
                mOnLiveSearchRecordCreateListener.recordCreated(row);
            }
        }
    }


    public interface OnLiveSearchRecordCreateListener {
        public void recordCreated(ODataRow row);
    }

}
