package com.odoo.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class ProductListBean extends BaseResponseBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Expose
	private List<Product> Product = new ArrayList<Product>();

	/**
	 * 
	 * @return The Product
	 */
	public List<Product> getProduct() {
		return Product;
	}

	/**
	 * 
	 * @param Product
	 *            The Product
	 */
	public void setProduct(List<Product> Product) {
		this.Product = Product;
	}

}