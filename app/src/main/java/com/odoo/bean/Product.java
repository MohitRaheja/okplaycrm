package com.odoo.bean;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;

public class Product implements Serializable, Parcelable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String productid;
	private String Name;
	private String Price;
	private String sale_price;
	private String Image;
	private String itemid;
	
	/**
	 * 
	 * @return The itemId
	 */
	public String getItemid() {
		return itemid;
	}

	/**
	 *
	 */
	public void setItemid(String itemid) {
		this.itemid = itemid;
	}
	
	/**
	 * 
	 * @return The productid
	 */
	public String getProductid() {
		return productid;
	}

	/**
	 * 
	 * @param productid
	 *            The productid
	 */
	public void setProductid(String productid) {
		this.productid = productid;
	}

	/**
	 * 
	 * @return The Name
	 */
	public String getName() {
		return Name;
	}

	/**
	 * 
	 * @param Name
	 *            The Name
	 */
	public void setName(String Name) {
		this.Name = Name;
	}

	/**
	 * 
	 * @return The Price
	 */
	public String getPrice() {
		return Price;
	}
	
	/**
	 * 
	 * @param Price
	 *            The Price
	 */
	public void setPrice(String Price) {
		this.Price = Price;
	}

	/**
	 * 
	 * @param sale_price
	 *            The Sale Price
	 */
	public void setSalePrice(String sale_price) {
		this.sale_price = sale_price;
	}
	
	/**
	 * 
	 * @return The Sale Price
	 */
	public String getSalePrice() {
		return sale_price;
	}

	/**
	 * 
	 * @return The Image
	 */
	public String getImage() {
		return Image;
	}

	/**
	 * 
	 * @param Image
	 *            The Image
	 */
	public void setImage(String Image) {
		this.Image = Image;
	}

	protected Product(Parcel in) {
		productid = in.readString();
		Name = in.readString();
		Price = in.readString();
		Image = in.readString();
		sale_price = in.readString();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(productid);
		dest.writeString(Name);
		dest.writeString(Price);
		dest.writeString(Image);
		dest.writeString(sale_price);
	}

	@SuppressWarnings("unused")
	public static final Creator<Product> CREATOR = new Creator<Product>() {
		@Override
		public Product createFromParcel(Parcel in) {
			return new Product(in);
		}

		@Override
		public Product[] newArray(int size) {
			return new Product[size];
		}
	};

}