package com.odoo.bean;

import java.io.Serializable;

public class ProductBean implements Serializable{
	private int id, pid;
	private String name, description, image;
	
	public void setParentId(int id){
		this.id = id;
	}
	public int getParentId(){
		return this.id;
	}
	
	public void setProductId(int pid){
		this.pid = pid;
	}
	
	public int getProductId(){
		return this.pid;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setDescription(String description){
		this.description = description;
	}
	
	public String getDescription(){
		return this.description;
	}
	
	public void setImageURL(String image){
		this.image = image;
	}
	
	public String getImageURL(){
		return this.image;
	}
	
}
