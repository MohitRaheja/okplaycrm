package com.odoo.bean;

import java.io.Serializable;

public class CategoriesBean implements Serializable{
	private int id;
	private String name;
	private boolean hasChild;
	
	public void setId(int id){
		this.id = id;
	}
	
	public int getId(){
		return this.id;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setChilds(boolean hasChild){
		this.hasChild = hasChild;
	}
	
	public boolean getChilds(){
		return this.hasChild;
	}

}
