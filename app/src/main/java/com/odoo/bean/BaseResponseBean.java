package com.odoo.bean;

import java.io.Serializable;

import com.google.gson.annotations.Expose;

public class BaseResponseBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Expose
	private String flag;
	@Expose
	private String Result;
	/**
	 * 
	 * @return The flag
	 */
	public String getFlag() {
		return flag;
	}

	/**
	 * 
	 * @param flag
	 *            The flag
	 */
	public void setFlag(String flag) {
		this.flag = flag;
	}

	/**
	 * 
	 * @return The Result
	 */
	public String getResult() {
		return Result;
	}

	/**
	 * 
	 * @param Result
	 *            The Result
	 */
	public void setResult(String Result) {
		this.Result = Result;
	}

}