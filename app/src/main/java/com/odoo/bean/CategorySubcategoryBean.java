package com.odoo.bean;

import java.io.Serializable;
import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class CategorySubcategoryBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String Category, CategoryId, breadcrumb;

	ArrayList<CategorySubcategoryBean> category = new ArrayList<CategorySubcategoryBean>();

	public void addCategory(CategorySubcategoryBean categoryItem) {
		category.add(categoryItem);
	}

	public ArrayList<CategorySubcategoryBean> getChildren() {
		return category;
	}

	public String getCategoryId() {
		return CategoryId;
	}

	public void setCategoryId(String categoryId) {
		CategoryId = categoryId;
	}

	public String getCategory() {
		return Category;
	}

	public void setCategory(String category) {
		Category = category;
	}
	
	public String getBreadcrumb() {
		return breadcrumb;
	}

	public void setBreadcrumb(String breadcrumb) {
		this.breadcrumb = breadcrumb;
	}
	
	/*public CategorySubcategoryBean()
	{
		
	}
	
	protected CategorySubcategoryBean(Parcel in) {
//		category = in.readArrayList(null);
		in.readTypedList(category, CategorySubcategoryBean.CREATOR);
		Category = in.readString();
		CategoryId = in.readString();
		breadcrumb = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Category);
        dest.writeString(CategoryId);
        dest.writeString(breadcrumb);
        dest.writeTypedList(category);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CategorySubcategoryBean> CREATOR = new Parcelable.Creator<CategorySubcategoryBean>() {
        @Override
        public CategorySubcategoryBean createFromParcel(Parcel in) {
            return new CategorySubcategoryBean(in);
        }

        @Override
        public CategorySubcategoryBean[] newArray(int size) {
            return new CategorySubcategoryBean[size];
        }
    };*/

}
