package com.odoo.quotations;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.odoo.R;
import com.odoo.core.support.addons.fragment.BaseFragment;
import com.odoo.core.support.drawer.ODrawerItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mohitraheja on 09/08/15.
 */
public class Quotations extends BaseFragment {

    public static final String KEY = Quotations.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.frag_quotations, container, false);
    }

    @Override
    public List<ODrawerItem> drawerMenus(Context context) {
        List<ODrawerItem> items = new ArrayList<ODrawerItem>();
        items.add(new ODrawerItem(KEY).setTitle("Quotations")
                .setIcon(R.drawable.ic_action_customers)
                .setInstance(new Quotations()));
        return items;
    }

    @Override
    public <T> Class<T> database() {
        return null;
    }
}
