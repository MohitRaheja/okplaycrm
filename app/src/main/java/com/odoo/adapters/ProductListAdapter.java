package com.odoo.adapters;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Environment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.odoo.R;
import com.odoo.bean.ProductBean;
import com.odoo.products.FullScreenImageActivity;
import com.odoo.products.VideoActivity;

import org.w3c.dom.Text;

public class ProductListAdapter extends BaseAdapter{

	ArrayList<ProductBean> products = null;
	Activity activity;
	private LayoutInflater inflater = null;
    public DisplayImageOptions baseImageoptions;

	public ProductListAdapter(Activity activity, ArrayList<ProductBean> list) {
		this.activity = activity;
		this.products = list;
		this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		initImageLoaderM();
	}

	@Override
	public int getCount() {
		if (products != null){
			return products.size();
		} else {
			return 0;
		}
	}

	@Override
	public ProductBean getItem(int position) {
		return products.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.product_list_row, parent, false);
			holder = new ViewHolder();
			holder.prod_name = (TextView) convertView.findViewById(R.id.product_name);
			holder.prod_image = (ImageView) convertView.findViewById(R.id.product_image);
            holder.play_video = (TextView) convertView.findViewById(R.id.play_video);
			convertView.setTag(holder);
		}else {
			holder = (ViewHolder) convertView.getTag();
		}

		final ProductBean obj = getItem(position);
		holder.prod_name.setText(obj.getName());

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showDialog(obj.getDescription());
			}
		});
		
		//String imageName = "image_" + obj.getProductId() +".jpg";
		File folder = new File(Environment.getExternalStorageDirectory().toString() + File.separator + "ImagesOkPlay" + File.separator
				+ obj.getProductId());
		File images[] = folder.listFiles();
		if(images.length > 0){
			String url = "file:///" + images[0].getPath();
			ImageLoader.getInstance().displayImage(url,holder.prod_image, baseImageoptions);
		}
		//String imageUrl = "assets://images/" + imageName;


		holder.prod_image.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(activity, FullScreenImageActivity.class);
                intent.putExtra("prod_id", obj.getProductId());
                activity.startActivity(intent);
			}
		});

        holder.play_video.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
				Intent intent = new Intent(activity, VideoActivity.class);
				intent.putExtra("prod_id", obj.getProductId());
				activity.startActivity(intent);
            }
        });

		return convertView;
	}

	private void showDialog(String desc)
	{
		final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.breadcrumb_dialog);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		Window window = dialog.getWindow();
		lp.copyFrom(window.getAttributes());
		//This makes the dialog take up the full width
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		window.setAttributes(lp);

		TextView header = (TextView) dialog.findViewById(R.id.dialog_header);
		header.setText("Description");

		LinearLayout main_lay = (LinearLayout) dialog.findViewById(R.id.main_lay);

		TextView txt = (TextView) ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
				.inflate(R.layout.spinner_textview, null, false);
		txt.setText(Html.fromHtml(desc));

		main_lay.addView(txt);

		dialog.show();
	}

	private class ViewHolder {

		TextView prod_name, play_video;
		ImageView prod_image;
	}

	public void initImageLoaderM(){
		baseImageoptions = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.app_icon)
				.showImageForEmptyUri(R.drawable.app_icon)
				.showImageOnFail(R.drawable.app_icon)
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(activity)
				.threadPriority(Thread.NORM_PRIORITY - 2)
				.denyCacheImageMultipleSizesInMemory()
				.diskCacheFileNameGenerator(new Md5FileNameGenerator())
				.diskCacheSize(5 * 1024 * 1024) // 50 Mb
				.tasksProcessingOrder(QueueProcessingType.LIFO)
				.build();
		ImageLoader.getInstance().init(config);
	}

}

