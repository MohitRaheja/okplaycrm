package com.odoo.products;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.odoo.R;

import java.util.ArrayList;

/**
 * @author Manoj Kumar
 * manoj[dot]kumar[at]letsgomo[dot]com
 */

public class FullScreenImagePagerAdapter extends PagerAdapter {

	private Activity activity;
	private ArrayList<String> imageArray;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	private int count = 0;
	private String[] pageIDsArray;
	private Bitmap bitmap;
	
	public FullScreenImagePagerAdapter(Activity activity, ArrayList<String> imgArray,
			ImageLoader imageLoader, DisplayImageOptions options) {
		this.activity = activity;
		this.imageArray = imgArray;
		this.imageLoader = imageLoader;
		this.options = options;
		
		Drawable d = activity.getResources().getDrawable(R.drawable.app_icon);
		this.bitmap = ((BitmapDrawable)d).getBitmap();
		
		int actualNoOfIDs = imageArray.size();
	    count = actualNoOfIDs + 2;
	    pageIDsArray = new String[count];
	    for (int i = 0; i < actualNoOfIDs; i++) {
	        pageIDsArray[i + 1] = imageArray.get(i);
	    }
	    pageIDsArray[0] = imageArray.get(actualNoOfIDs - 1);
	    pageIDsArray[count - 1] = imageArray.get(0);
	}

	public int getCount() {
		if (imageArray.size()==1) {
			return 1;
		}else{
			return count;
		}
	}

	public Object instantiateItem(View collection, int position) {
		View view = activity.getLayoutInflater().inflate(R.layout.item_full_screen_activity, null);
		ImageView img = (ImageView) view.findViewById(R.id.article_image_zoom);
		imageLoader.displayImage(pageIDsArray[position], img, options, new ImageLoadingListener() {
			public void onLoadingStarted(String imageUri, View view) {
				if (view!=null && bitmap!=null) 
					((ImageView)view).setImageBitmap(bitmap);
			}
			
			public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
				if (view!=null && bitmap!=null) 
					((ImageView)view).setImageBitmap(bitmap);
			}
			
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			}
			
			public void onLoadingCancelled(String imageUri, View view) {
			}
		});
		((ViewPager) collection).addView(img, 0);

		return img;
	}

	@Override
	public void destroyItem(View arg0, int arg1, Object arg2) {
		((ViewPager) arg0).removeView((View) arg2);
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == ((View) arg1);
	}

	@Override
	public Parcelable saveState() {
		return null;
	}
}