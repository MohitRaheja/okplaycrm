package com.odoo.products;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.odoo.R;

import java.io.File;
import java.util.ArrayList;

/**
 * @author Manoj Kumar
 * manoj[dot]kumar[at]letsgomo[dot]com
 */

public class FullScreenImageActivity extends Activity {
	
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	
	private TextView close_image;
	private int prod_id;
	private ArrayList<String> mImageArray;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.full_screen_image_activity);
		
		Intent intent = getIntent();
		if(intent != null) {
			Bundle extras = intent.getExtras();
			if(extras != null) {
				prod_id = extras.getInt("prod_id");
			}
		}else{
			finish();
		}
		
		initImageLoader();

        mImageArray = new ArrayList<String>();
		File folder = new File(Environment.getExternalStorageDirectory().toString() + File.separator + "ImagesOkPlay" + File.separator
						+ prod_id);

		File images[] = folder.listFiles();
		for (File f: images){
            mImageArray.add("file:///" + f.getPath());
            Log.e("path,..........", "file:///" + f.getPath());
		}
		
		final ViewPager main_image_pager = (ViewPager) findViewById(R.id.main_image_pager);
		final FullScreenImagePagerAdapter adapter = new FullScreenImagePagerAdapter(this, mImageArray, imageLoader,options);
		main_image_pager.setAdapter(adapter);
		
		final int pageCount = adapter.getCount();
        /*if (mPosition == 0){
        	main_image_pager.setCurrentItem(pageCount-2,false);
        } else if (mPosition == pageCount-1){
        	main_image_pager.setCurrentItem(1,false);
        }else{
        	main_image_pager.setCurrentItem(mPosition,false);
        }*/
        
        main_image_pager.setOnPageChangeListener(new OnPageChangeListener() {
			public void onPageSelected(final int arg0) {
				final int pageCount = adapter.getCount();
				if (pageCount > 1) {
					if (arg0 == 0){
						main_image_pager.setCurrentItem(pageCount-2,false);
					} else if (arg0 == pageCount-1){
						main_image_pager.setCurrentItem(1,false);
					}
				}
			}
			
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}
			
			public void onPageScrollStateChanged(int arg0) {
			}
		});
        
		close_image = (TextView) findViewById(R.id.close_image);
	
		close_image.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				FullScreenImageActivity.this.finish();
			}
		});
	}
	private void initImageLoader() {
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder().showImageForEmptyUri(R.drawable.app_icon).showImageOnFail(R.drawable.app_icon).resetViewBeforeLoading(true).cacheOnDisc(true)
				.imageScaleType(ImageScaleType.EXACTLY_STRETCHED).bitmapConfig(Bitmap.Config.RGB_565)/*.considerExifParams(true)*/.displayer(new FadeInBitmapDisplayer(300)).build();
		imageLoader.init(ImageLoaderConfiguration.createDefault(FullScreenImageActivity.this));
	}

}
