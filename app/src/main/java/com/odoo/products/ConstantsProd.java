package com.odoo.products;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * Class to Hold the Constants
 * */
public class ConstantsProd {
	
	public static final String CATEGORY_TABLE = "category";
	
	public static final String SUB_CATEGORY_TABLE = "subCategory";
	
	public static final String SUB_CATEGORY_PRODUCT_TABLE = "subCategoryProduct";

}
