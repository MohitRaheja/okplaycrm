package com.odoo.products;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.Window;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.odoo.R;

import java.io.File;

/**
 * Created by mohitraheja on 21/08/15.
 */
public class VideoActivity extends Activity {

    VideoView video;
    private int prod_id;
    String url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_video);

        video = (VideoView) findViewById(R.id.myvideoview);
        Intent intent = getIntent();
        if(intent != null) {
            Bundle extras = intent.getExtras();
            if(extras != null) {
                prod_id = extras.getInt("prod_id");
            }
        }else{
            finish();
        }

        File folder = new File(Environment.getExternalStorageDirectory().toString() + File.separator + "VideosOkPlay" + File.separator
                + prod_id);

        File images[] = folder.listFiles();
        if(images != null && images.length > 0){
            url = images[0].getPath();
        }
        else {
            Toast.makeText(this, "No videos found", Toast.LENGTH_LONG).show();
            finish();
        }

        final ProgressDialog progressDialog = ProgressDialog.show(this, "", "Loading...", true);
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(video);
        video.setMediaController(mediaController);
        video.setVideoPath(url);

        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            public void onPrepared(MediaPlayer arg0) {
                progressDialog.dismiss();
                video.start();
            }
        });
    }
}
