package com.odoo.products;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.odoo.OdooActivity;
import com.odoo.R;
import com.odoo.adapters.HomeListAdapter;
import com.odoo.adapters.ProductListAdapter;
import com.odoo.bean.CategoriesBean;
import com.odoo.bean.Product;
import com.odoo.bean.ProductBean;
import com.odoo.core.support.addons.fragment.BaseFragment;
import com.odoo.core.support.drawer.ODrawerItem;
import com.odoo.core.utils.OFragmentUtils;
import com.odoo.database.DataAdapter;

public class HomeScreen extends BaseFragment implements OnItemClickListener{
	private ArrayList<CategoriesBean> catObj, subCatObj;
	private HomeListAdapter mAdapter;
	private int position = 0;
	
	private LinearLayout cat_main_layout;
	private ScrollView scroll_view;
	private ListView sub_cat_listView;
	private ImageView []arrowImageArray;
	private ImageView []catImageArray;
	View mView;
    boolean isLoaded = false;

    public static final String KEY = HomeScreen.class.getSimpleName();

	@Override
	public View onCreateView(LayoutInflater inflater,
							 ViewGroup container, Bundle savedInstanceState) {
        if(!isLoaded) {
            setHasOptionsMenu(true);
            //setHasSyncStatusObserver(KEY, this, db());
            return inflater.inflate(R.layout.s_home_screen, container, false);
        }
        else
            return mView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

        if(!isLoaded) {
            Bundle bundle = getArguments();
            if (bundle != null) {
                catObj = (ArrayList<CategoriesBean>) bundle.getSerializable("Categories");
            }
            mView = view;
        }
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

        if(!isLoaded) {
            cat_main_layout = (LinearLayout) mView.findViewById(R.id.cat_main_layout);
            scroll_view = (ScrollView) mView.findViewById(R.id.scroll_view);
            sub_cat_listView = (ListView) mView.findViewById(R.id.cat_list);
            sub_cat_listView.setOnItemClickListener(this);

            if ((catObj.get(0).getChilds())) {
                getChildrens(catObj.get(0).getId());
            }

            LayoutInflater inflater = getActivity().getLayoutInflater();
            arrowImageArray = new ImageView[catObj.size()];
            catImageArray = new ImageView[catObj.size()];

            for (int i = 0; i < catObj.size(); i++) {
                View view = inflater.inflate(R.layout.item_cat_main, null);
                catImageArray[i] = (ImageView) view.findViewById(R.id.cat_icon);
                TextView cat_name = (TextView) view.findViewById(R.id.cat_name);
                ImageView indicator = (ImageView) view.findViewById(R.id.indicator);
                catImageArray[i].setImageResource(getImageResource(catObj.get(i).getName().toLowerCase().trim()));
                cat_name.setText(catObj.get(i).getName());
                arrowImageArray[i] = indicator;
                if (i != 0) {
                    arrowImageArray[i].setVisibility(View.INVISIBLE);
                    catImageArray[i].setSelected(false);
                } else {
                    arrowImageArray[i].setVisibility(View.VISIBLE);
                    catImageArray[i].setSelected(true);
                }
                view.setTag(i);
                view.setOnClickListener(listener);
                cat_main_layout.addView(view);
            }
        }
	}

	/*@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.s_home_screen);
		
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			catObj = (ArrayList<CategoriesBean>) bundle.getSerializable("Categories");
		}
		*//*else{
			String response = UtilityMethods.readCategoryResponse(this, Constants.categoriesFile);
			catObj = UtilityMethods.getCategorySubCategory(response);
		}*//*

		cat_main_layout = (LinearLayout) findViewById(R.id.cat_main_layout);
		scroll_view = (ScrollView) findViewById(R.id.scroll_view);
		sub_cat_listView = (ListView) findViewById(R.id.cat_list);
		sub_cat_listView.setOnItemClickListener(this);
		
		if((catObj.get(0).getChilds())){
				getChildrens(catObj.get(0).getId());
		}

		LayoutInflater inflater = this.getLayoutInflater();
		arrowImageArray = new ImageView[catObj.size()];
		catImageArray = new ImageView[catObj.size()];
		
		for (int i = 0; i < catObj.size(); i++) {
			View view = inflater.inflate(R.layout.item_cat_main, null);
			catImageArray[i] = (ImageView) view.findViewById(R.id.cat_icon);
			TextView cat_name = (TextView) view.findViewById(R.id.cat_name);
			ImageView indicator = (ImageView) view.findViewById(R.id.indicator);
			catImageArray[i].setImageResource(getImageResource(catObj.get(i).getName().toLowerCase().trim()));
			cat_name.setText(catObj.get(i).getName());
			arrowImageArray[i] = indicator;
			if (i != 0) {
				arrowImageArray[i].setVisibility(View.INVISIBLE);
				catImageArray[i].setSelected(false);
			} else {
				arrowImageArray[i].setVisibility(View.VISIBLE);
				catImageArray[i].setSelected(true);
			}
			view.setTag(i);
			view.setOnClickListener(listener);
			cat_main_layout.addView(view);
		}
		
//		initHeader(findViewById(R.id.header), true, null);
//		setCartCount();
	}*/
	
	private int getImageResource(String name){
		name = name.replaceAll(" ", "_");
		name = name.replaceAll("-", "_");
		name = name.toLowerCase();
		if (name.contains("fruits")) {
			name = "fruits";
		}else if (name.contains("dairy")) {
			name = "dairy";
		}

		int resId = 0;
		resId = getResources().getIdentifier("cat_"+name, "drawable", getActivity().getApplicationInfo().packageName);
		if (resId == 0) {
			resId = getResources().getIdentifier("cat_staples", "drawable", getActivity().getApplicationInfo().packageName);
		}
		return resId;
	}
	
	OnClickListener listener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			position = (Integer) view.getTag();
			int id = catObj.get(position).getId();
			if(catObj.get(position).getChilds()){
				getChildrens(id);
				hideAllImage();
				arrowImageArray[position].setVisibility(View.VISIBLE);
				catImageArray[position].setSelected(true);
			}
			else
				getProducts(id);
			//scroll_view.scrollTo(0, 0);
		}
	};
	
	private void getProducts(int id){
		String query = "SELECT * FROM " + ConstantsProd.SUB_CATEGORY_PRODUCT_TABLE + " WHERE pid = " + id;
        new GetData("Products", query).execute();
//		myApi.reqProducts(query, MyReceiverActions.PRODUCTS);
	}
	
	private void getChildrens(int id)
	{
		String query = "SELECT * FROM " + ConstantsProd.SUB_CATEGORY_TABLE + " WHERE pid = " + id;
        new GetData("Category", query).execute();
//		myApi.reqCategories(query, MyReceiverActions.SUB_CATEGORIES);
	}
	
	private void hideAllImage() {
		for (int i = 0; i < arrowImageArray.length; i++) {
			arrowImageArray[i].setVisibility(View.INVISIBLE);
			catImageArray[i].setSelected(false);
		}
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
		getProducts(subCatObj.get(pos).getId());
	}

	@Override
	public List<ODrawerItem> drawerMenus(Context context) {
		List<ODrawerItem> items = new ArrayList<ODrawerItem>();
		items.add(new ODrawerItem(KEY).setTitle("Products")
				.setIcon(R.drawable.ic_action_customers)
				.setInstance(new HomeScreen()));
		return items;
	}

	@Override
	public <T> Class<T> database() {
		return null;
	}

	class GetData extends AsyncTask<Void, Void, Object>{

        String type, query;
        ProgressDialog dialog;
        GetData(String type, String query){
            this.type = type;
            this.query = query;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Object doInBackground(Void... params) {

            DataAdapter mDbHelper = new DataAdapter(getActivity().getApplicationContext());
            mDbHelper.createDatabase();
            mDbHelper.open();

            Cursor data = mDbHelper.getData(query);
            ArrayList<ProductBean> products;
            if(type.equals("Category"))
                subCatObj = parseCategoryResponse(data);
            else {
                products = parseProductsResponse(data);
                return products;
            }
            data.close();

            mDbHelper.close();

            return null;
        }

        public ArrayList<CategoriesBean> parseCategoryResponse(Cursor cursor){
            try {
                ArrayList<CategoriesBean> catList = new ArrayList<CategoriesBean>();
                cursor.moveToFirst();
                do {
                    CategoriesBean cat = new CategoriesBean();
                    cat.setId(cursor.getInt(0));
                    cat.setName(cursor.getString(1));
                    cat.setChilds(cursor.getInt(2)>0);
                    catList.add(cat);
                } while (cursor.moveToNext());
                return catList;
            } catch (Exception e) {
                Log.e("tle99", e.getMessage());
            }
            return null;
        }

        public ArrayList<ProductBean> parseProductsResponse(Cursor cursor){
            try {
                ArrayList<ProductBean> productList = new ArrayList<ProductBean>();
                cursor.moveToFirst();
                do {
                    ProductBean product = new ProductBean();
                    product.setProductId(cursor.getInt(0));
                    product.setName(cursor.getString(1));
                    product.setImageURL(cursor.getString(2));
                    product.setDescription(cursor.getString(3));
                    product.setParentId(cursor.getInt(4));
                    productList.add(product);
                } while (cursor.moveToNext());
                return productList;
            } catch (Exception e) {
                Log.e("tle99", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            if(dialog.isShowing())
                dialog.dismiss();

            if(type.equals("Category")) {
                if (mAdapter != null)
                    mAdapter.refreshList(subCatObj);
                else {
                    mAdapter = new HomeListAdapter(getActivity(), subCatObj);
                    sub_cat_listView.setAdapter(mAdapter);
                }
            }
            else{
                ArrayList<ProductBean> bean = (ArrayList<ProductBean>) o;
				ProductListScreen instance = new ProductListScreen();
				Bundle extra = new Bundle();
                extra.putSerializable("ProductList", bean);
                extra.putSerializable("Header", "Products");
                ((OdooActivity) getActivity()).loadFragment(instance, true, extra);
                isLoaded = true;
                /*Intent i = new Intent(getActivity(), ProductListScreen.class);
                i.putExtra("ProductList", bean);
                i.putExtra("Header", "Products");
                startActivity(i);*/
            }

        }
    }
}
