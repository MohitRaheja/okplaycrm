package com.odoo.products;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.odoo.R;
import com.odoo.adapters.ProductListAdapter;
import com.odoo.bean.Product;
import com.odoo.bean.ProductBean;
import com.odoo.core.support.addons.fragment.BaseFragment;
import com.odoo.core.support.drawer.ODrawerItem;

public class ProductListScreen extends BaseFragment {
	int currentFirstVisibleItem = 0;
	int currentVisibleItemCount = 10;
	int totalItemCount = 10;
	boolean isLoading = false;
	int itemPerPage = 10;
	boolean hasMoreItem = true;

	private String header;
	private ListView mList;
	ProductListAdapter mAdapter;
	private Product product;
	public int pageNo = 1;
	String cat_id = "";
	ArrayList<ProductBean> product_list;
	View footerView;
    public static final String KEY = ProductListScreen.class.getSimpleName();
	
	LinearLayout ll_brad_crum;
	HorizontalScrollView hscrollview;
	View hrc;
	String quantity = "", product_id = "";
	String page;
    View mView;

	@Override
	public View onCreateView(LayoutInflater inflater,
							 ViewGroup container, Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		//setHasSyncStatusObserver(KEY, this, db());
		return inflater.inflate(R.layout.activity_categoty_list, container, false);
	}

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            product_list = (ArrayList<ProductBean>) bundle
                    .getSerializable("ProductList");
            try {
                header = bundle.getString("Header");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mView = view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mList = (ListView) mView.findViewById(R.id.category_list);
        mAdapter = new ProductListAdapter(getActivity(), product_list);
        mList.setAdapter(mAdapter);
    }

    //    @Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//
//		Bundle bundle = getIntent().getExtras();
//		if (bundle != null) {
//			product_list = (ArrayList<ProductBean>) bundle
//					.getSerializable("ProductList");
//			try {
//				header = bundle.getString("Header");
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//		setContentView(R.layout.activity_categoty_list);
//
////		addActionsInFilter(MyReceiverActions.PRODUCT_CONTENT_LIST);
////		addActionsInFilter(MyReceiverActions.PRODUCT_LIST);
////		addActionsInFilter(MyReceiverActions.ADD_TO_CART);
////		addActionsInFilter(MyReceiverActions.ADD_PRODUCT_TO_LIST);
////		addActionsInFilter(MyReceiverActions.SHOPPING_LIST_PRODUCT);
//
//		mList = (ListView) findViewById(R.id.category_list);
//		mAdapter = new ProductListAdapter(ProductListScreen.this, product_list);
//		mList.setAdapter(mAdapter);
//
////		initHeader(findViewById(R.id.header), true, header);
//	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	
//	@Override
//	protected void onDestroy() {
//		super.onDestroy();
//	}

	@Override
	public List<ODrawerItem> drawerMenus(Context context) {
        List<ODrawerItem> items = new ArrayList<ODrawerItem>();
        items.add(new ODrawerItem(KEY).setTitle("Products")
                .setIcon(R.drawable.ic_action_customers)
                .setInstance(new ProductListScreen()));
        return items;
	}

	@Override
	public <T> Class<T> database() {
		return null;
	}
}
