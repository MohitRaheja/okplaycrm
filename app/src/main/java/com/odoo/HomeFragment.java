package com.odoo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.odoo.addons.calendar.CalendarDashboard;
import com.odoo.addons.customers.Customers;
import com.odoo.addons.dsr.DSR;
import com.odoo.core.utils.OFragmentUtils;
import com.odoo.products.ConstantsProd;
import com.odoo.products.HomeScreen;

/**
 * Created by mohitraheja on 01/10/15.
 */
public class HomeFragment  extends Fragment implements View.OnClickListener{

    View mView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.home_screen, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        TextView customers = (TextView) mView.findViewById(R.id.customers);
        TextView calendar = (TextView) mView.findViewById(R.id.calendar);
        TextView products = (TextView) mView.findViewById(R.id.products);
        TextView dsr = (TextView) mView.findViewById(R.id.dsr);
        customers.setOnClickListener(this);
        calendar.setOnClickListener(this);
        products.setOnClickListener(this);
        dsr.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Fragment frag = null;
        switch(v.getId()){
            case R.id.customers:
                frag = new Customers();
                break;
            case R.id.calendar:
                frag = new CalendarDashboard();
                break;
            case R.id.products:
                frag = new HomeScreen();
                break;
            case R.id.dsr:
                frag = new DSR();
                break;

        }

        if( frag != null && frag instanceof HomeScreen){
            String query = query = "SELECT * FROM " + ConstantsProd.CATEGORY_TABLE;
            ((OdooActivity)getActivity()).new GetData(query, frag).execute();
        }
        else{
            ((OdooActivity)getActivity()).replaceFragment(frag, null);
        }

    }
}
